const _ = require('lodash');
const moment = require('moment');

const stats = require('../../core/stats');
const util = require('../../core/util');
var request = require('request'); // Librería para enviar petición
const configUtil = require('../../web/vue/UIconfig'); // configuración incial

const ENV = util.gekkoEnv();

const config = util.getConfig();
const perfConfig = config.performanceAnalyzer;
const watchConfig = config.watch;

// Libreria para crear archivo
const fs = require('fs');
// Load the proper module that handles the results
var Handler;
if(ENV === 'child-process')
  Handler = require('./cpRelay');
else
  Handler = require('./logger');

const PerformanceAnalyzer = function() {
  _.bindAll(this);

  this.dates = {
    start: false,
    end: false
  }

  this.startPrice = 0;
  this.endPrice = 0;

  this.currency = watchConfig.currency;
  this.asset = watchConfig.asset;

  this.handler = new Handler(watchConfig);

  this.trades = 0;

  this.sharpe = 0;

  this.roundTrips = [];
  this.roundTrip = {
    id: 0,
    entry: false,
    exit: false
  }
}

PerformanceAnalyzer.prototype.processCandle = function(candle, done) {
  this.price = candle.close;
  this.dates.end = candle.start;

  if(!this.dates.start) {
    this.dates.start = candle.start;
    this.startPrice = candle.close;
  }

  this.endPrice = candle.close;

  done();
}

PerformanceAnalyzer.prototype.processPortfolioUpdate = function(portfolio) {
  this.start = portfolio;
  this.current = _.clone(portfolio);
}

PerformanceAnalyzer.prototype.processTrade = function(trade) {
  this.trades++;
  this.current = trade.portfolio;

  const report = this.calculateReportStatistics();
  this.handler.handleTrade(trade, report);

  this.logRoundtripPart(trade);
}

PerformanceAnalyzer.prototype.logRoundtripPart = function(trade) {
  var dir = './logs/logRoundtripPart/';
  // if (!fs.existsSync(dir)){
  //   fs.mkdirSync(dir);
  // }
  // fs.appendFile('./logs/logRoundtripPart/trades.txt', JSON.stringify(trade, null, 2) + '\r\n' + '--------------------------------------------------------------------------------------------------------------------------' + '\r\n' , 'utf-8');
  // fs.appendFile('./logs/logRoundtripPart/this.txt', JSON.stringify(this, null, 2) + '\r\n' + '--------------------------------------------------------------------------------------------------------------------------' + '\r\n' , 'utf-8');

  // this is not part of a valid roundtrip
  if(!this.roundTrip.entry && trade.action === 'sell') {
    return;
  }
  this.roundTrip.botId = trade.botId;
  this.roundTrip.botPid = trade.botPid;
  this.roundTrip.orders = trade.orders;

  if(trade.action === 'buy') {
    if (this.roundTrip.exit) {
      this.roundTrip.id++;
      this.roundTrip.exit = false
    }

    this.roundTrip.entry = {
      date: trade.date,
      price: trade.price,
      total: trade.portfolio.currency + (trade.portfolio.asset * trade.price),
    }
  } else if(trade.action === 'sell') {
    this.roundTrip.exit = {
      date: trade.date,
      price: trade.price,
      total: trade.portfolio.currency + (trade.portfolio.asset * trade.price),
    }

    // fs.appendFile('./logs/logRoundtripPart/thisRoundtrip.txt', JSON.stringify(this.roundTrip, null, 2) + '\r\n' + '--------------------------------------------------------------------------------------------------------------------------' + '\r\n' , 'utf-8');
    this.handleRoundtrip();
  }
}

PerformanceAnalyzer.prototype.round = function(amount) {
  return amount.toFixed(8);
}

PerformanceAnalyzer.prototype.handleRoundtrip = function() {
  var roundtrip = {
    id: this.roundTrip.id,
    botId: this.roundTrip.botId,
    botPid: this.roundTrip.botPid,
    orders: this.roundTrip.orders,
    entryAt: this.roundTrip.entry.date,
    entryPrice: this.roundTrip.entry.price,
    entryBalance: this.roundTrip.entry.total,

    exitAt: this.roundTrip.exit.date,
    exitPrice: this.roundTrip.exit.price,
    exitBalance: this.roundTrip.exit.total,

    duration: this.roundTrip.exit.date.diff(this.roundTrip.entry.date)
  }

  roundtrip.pnl = roundtrip.exitBalance - roundtrip.entryBalance;
  roundtrip.profit = (100 * roundtrip.exitBalance / roundtrip.entryBalance) - 100;

  // Se envía a la api de php si el roundtrip tiene botID
  if (roundtrip.botId) {
    this.sendRountripAPI(roundtrip);
  }

  this.roundTrips[this.roundTrip.id] = roundtrip;

  var dir1 = './logs/handleRoundtrip/';
  if (!fs.existsSync(dir1)) {
    fs.mkdirSync(dir1);
  }
  let obj = this;
  fs.appendFile('./logs/handleRoundtrip/roundtrips.txt', JSON.stringify(obj, null, 2) + '\r\n' + '--------------------------------------------------------------------------------------------------------------------------' + '\r\n' , 'utf-8');


  // this will keep resending roundtrips, that is not ideal.. what do we do about it?
  this.handler.handleRoundtrip(roundtrip);

  // we need a cache for sharpe

  // every time we have a new roundtrip
  // update the cached sharpe ratio
  this.sharpe = stats.sharpe(
    this.roundTrips.map(r => r.profit),
    perfConfig.riskFreeReturn
  );
}

// Permite hacer la petición de roundtrip a la api de php
PerformanceAnalyzer.prototype.sendRountripAPI = function(roundtrip) {
  let roundtripToSend = {
    id: roundtrip.id,
    botId: roundtrip.botId,
    botPid: roundtrip.botPid,
    orders: roundtrip.orders,
    entryAt: roundtrip.entryAt.toDate(),
    entryPrice: roundtrip.entryPrice,
    entryBalance: roundtrip.entryBalance,
    exitAt: roundtrip.exitAt.toDate(),
    exitPrice: roundtrip.exitPrice,
    exitBalance: roundtrip.exitBalance,
    duration: roundtrip.duration,
    pnl: roundtrip.pnl,
    profit: roundtrip.profit
  };

  request.post({url: configUtil.php.roundtrip, form: roundtripToSend}, function(err, httpResponse, body) {
    if (err) {
      return console.error('error', err);
    }
    console.log('Respuesta php ', body);
  });
}

PerformanceAnalyzer.prototype.calculateReportStatistics = function() {
  // the portfolio's balance is measured in {currency}
  let balance = this.current.currency + this.price * this.current.asset;
  let profit = balance - this.start.balance;

  let timespan = moment.duration(
    this.dates.end.diff(this.dates.start)
  );
  let relativeProfit = balance / this.start.balance * 100 - 100

  let report = {
    currency: this.currency,
    asset: this.asset,

    startTime: this.dates.start.utc().format('YYYY-MM-DD HH:mm:ss'),
    endTime: this.dates.end.utc().format('YYYY-MM-DD HH:mm:ss'),
    timespan: timespan.humanize(),
    market: this.endPrice * 100 / this.startPrice - 100,

    balance: balance,
    profit: profit,
    relativeProfit: relativeProfit,

    yearlyProfit: this.round(profit / timespan.asYears()),
    relativeYearlyProfit: this.round(relativeProfit / timespan.asYears()),

    startPrice: this.startPrice,
    endPrice: this.endPrice,
    trades: this.trades,
    startBalance: this.start.balance,
    sharpe: this.sharpe
  }

  report.alpha = report.profit - report.market;

  return report;
}

PerformanceAnalyzer.prototype.finalize = function(done) {
  const report = this.calculateReportStatistics();
  this.handler.finalize(report);
  done();
}


module.exports = PerformanceAnalyzer;
